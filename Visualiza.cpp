#include <pcl/visualization/cloud_viewer.h>
#include <iostream>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <fstream>
#include <sstream>
#include <string>
#include<vector>
#include<pcl/registration/transformation_estimation_svd.h>
#include<pcl/registration/transformation_estimation_svd_scale.h>
#include<pcl/registration/transformation_estimation_lm.h>
#include <pcl/sample_consensus/sac_model_registration.h>
#include <pcl/sample_consensus/ransac.h>

using namespace std;
using namespace pcl;
using namespace pcl::registration;
using namespace Eigen;


void VisualizePts(char* filename1, char* filename2)
{
    int ptNum1=0;
    vector<vector<double> > pts;
    if(filename1!="")
    {
        ifstream myfile(filename1);
        string line;
        while(getline(myfile,line))
        {
            istringstream is(line);
            vector<double> tmp(3);
            is>>tmp[0]>>tmp[1]>>tmp[2];
            pts.push_back(tmp);
            ptNum1++;
        }
        myfile.close();
    }
    if(filename2!="")
    {
        ifstream myfile(filename2);
        string line;
        while(getline(myfile,line))
        {
            istringstream is(line);
            vector<double> tmp(3);
            is>>tmp[0]>>tmp[1]>>tmp[2];
            pts.push_back(tmp);
        }
        myfile.close();
    }
    cout<<pts.size()<<endl;
    pcl::PointCloud<pcl::PointXYZRGB> cloud;
    cloud.is_dense = true;
    cloud.points.resize (pts.size());

    for (size_t i = 0; i < cloud.points.size (); ++i)
    {
        cloud.points[i].x = pts[i][0];
        cloud.points[i].y = pts[i][1];
        cloud.points[i].z = pts[i][2];
        uint8_t r,g,b;
        if(i<ptNum1)
        {
            r=255;
            g=0;
            b=0;
        }
        else
        {
            r=255;
            g=255;
            b=255;
        }
        uint32_t rgb = ((uint32_t)r << 16 | (uint32_t)g << 8 | (uint32_t)b);
        cloud.points[i].rgb=*reinterpret_cast<float*>(&rgb);
    }
    pcl::visualization::CloudViewer viewer ("Simple Cloud Viewer");
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr ptrCloud(&cloud);
    viewer.showCloud (ptrCloud);
    while (!viewer.wasStopped ())
    {
    }

}

int main(int argc, char* argv[])
{
    if(argc<3)return (0);
    char* filename1=argv[1],*filename2=argv[2];
    VisualizePts(filename1,filename2);
    int a;
    cin>>a;
    return (0);
}
