function [ flag ] = CropAll( folder_name,h )
files=dir(strcat(folder_name,'/*.png'));
for i=1:numel(files)
    file_name=files(i).name;
    img=imread([folder_name,'/',file_name]);
    img2=img(1:h,:,:);
    imwrite(img2,[folder_name,'/',file_name]);
end
flag=1;
end

